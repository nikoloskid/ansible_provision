# Ansible Provision Controller

### Quick intro to running Playbooks
```bash
# How to install kafka
target=company_x group=kafka ap -i hosts/company_x/ playbooks/kafka/setup.yml

# How to run performance test for kafka
target=company_y group=kafka ap -i hosts/company_y/ playbooks/kafka/performance_test.yml
```

## Editor
VS Code == much easier life
Run your first Ansible Playbook:
1. open VS Code
2. Install "Remote - Containers" extension
3. Open the project with `code .` and on the right bottom corner click "Reopen in Container"
   This will install ansible inside docker with all the settings and VS Code extensions
5. Go to playbooks/ folder and execute `ap ping.yml`

# Security
- Passwords, ssh keys and other sensitive data should go to ansible-vault encrypted file.
- Don't use `host_key_checking = false` in production inventory file.
- Logging in with root user is not allowed. Root user must be disabled.
- You can connect to machine only via ssh key/pair.
- use `ansible-vault edit <file>` to change the file. Never decrypt the secret file


# Testing
When you create or change existing Ansible playbook you should use vagrant to check if
you have made any mistakes. To create new cluster go to /ansible/vagrant/cluster and run
```bash
vagrant up
```
