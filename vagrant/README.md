# Vagrant

Every new Ansible playbook should be written and tested in clean virtual machine.

```bash
# create single machine go to /ansible/vagrant/single
vagrant up

# destroy virtual machine
vagrant destroy

# list all virtual machines
vagrant global-status
```
