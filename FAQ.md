# FAQ - Frequently Asked Questions

### `systemctl start <service_name>` doesn't start the service.
- Use `journalctl` to query the systemd journal
- sometimes this issue is because the system is out of disk. user `df -h`
