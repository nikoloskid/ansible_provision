# Note: You can use any Debian/Ubuntu based image you want.
FROM mcr.microsoft.com/vscode/devcontainers/base:bullseye

# [Option] Install zsh
ARG INSTALL_ZSH="true"
# [Option] Upgrade OS packages to their latest versions
ARG UPGRADE_PACKAGES="false"
# [Option] Enable non-root Docker access in container
ARG ENABLE_NONROOT_DOCKER="true"
# [Option] Use the OSS Moby CLI instead of the licensed Docker CLI
ARG USE_MOBY="true"

# Enable new "BUILDKIT" mode for Docker CLI
ENV DOCKER_BUILDKIT=1

# Install needed packages and setup non-root user. Use a separate RUN statement to add your
# own dependencies. A user of "automatic" attempts to reuse an user ID if one already exists.
ARG USERNAME=automatic
ARG USER_UID=1000
ARG USER_GID=$USER_UID
COPY library-scripts/*.sh /tmp/library-scripts/
RUN apt-get update \
    && /bin/bash /tmp/library-scripts/common-debian.sh "${INSTALL_ZSH}" "${USERNAME}" "${USER_UID}" "${USER_GID}" "${UPGRADE_PACKAGES}" "true" "true" \
    # Use Docker script from script library to set things up
    && /bin/bash /tmp/library-scripts/docker-debian.sh "${ENABLE_NONROOT_DOCKER}" "/var/run/docker-host.sock" "/var/run/docker.sock" "${USERNAME}" \
    # Clean up
    && apt-get autoremove -y && apt-get clean -y && rm -rf /var/lib/apt/lists/* /tmp/library-scripts/


RUN apt update
RUN apt install -y neovim ansible-lint python3-pip python3-docker iputils-ping net-tools traceroute
RUN ANSIBLE_SKIP_CONFLICT_CHECK=1 pip install ansible==4.6.0
RUN apt install -y sshpass
RUN apt install -y tree sudo yamllint jq curl

RUN ansible-galaxy collection install community.docker

RUN mkdir /home/vscode/.docker/

RUN echo "change-me" > /home/vscode/ansible-vault-password

# RUN echo "alias a='ansible'" >> ~/.zshrc
# RUN echo "alias ap='ansible-playbook'" >> ~/.zshrc
# RUN echo "alias av='ansible-vault'" >> ~/.zshrc
# RUN echo "alias ag='ansible-galaxy'" >> ~/.zshrc
# RUN echo "alias ai='ansible-inventory'" >> ~/.zshrc
# RUN echo "alias ailist='ansible-inventory --list --yaml'" >> ~/.zshrc
# RUN echo "alias afact='ansible -m setup --tree out'" >> ~/.zshrc
# RUN echo "alias afact-all='ansible -m setup all'" >> ~/.zshrc
# RUN echo "alias agroup=\"ansible controller -m debug -a 'var=groups.keys()'\"" >> ~/.zshrc
# RUN echo "alias ahost='ansible all --list-hosts'" >> ~/.zshrc


# Setting the ENTRYPOINT to docker-init.sh will configure non-root access
# to the Docker socket. The script will also execute CMD as needed.
ENTRYPOINT [ "/usr/local/share/docker-init.sh" ]
CMD [ "sleep", "infinity" ]

# [Optional] Uncomment this section to install additional OS packages.
# RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
#     && apt-get -y install --no-install-recommends <your-package-list-here>
